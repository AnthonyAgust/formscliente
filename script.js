document.addEventListener("DOMContentLoaded", function () {
    const form = document.getElementById("forms");
    const info = document.getElementById("info");

    form.addEventListener("submit", function (e) {
        e.preventDefault();

        const nombre = document.getElementById("nombre").value;
        const apellido = document.getElementById("apellido").value;
        const email = document.getElementById("email").value;
        const telefono = document.getElementById("telefono").value;
        const pass = document.getElementById("password").value;

        info.innerHTML = `
            <h2>Información del Cliente:</h2>
            <p><strong>Nombre:</strong> ${nombre}</p>
            <p><strong>Apellido:</strong> ${apellido}</p>
            <p><strong>Email:</strong> ${email}</p>
            <p><strong>Teléfono:</strong> ${telefono}</p>
            <p><strong>Contraseña:</strong> ${pass}</p>

            <br>
            <p><i>Nota: Por favor, comprueba que tus datos sean correctos y no contengan errores tipográficos.</i></p>
            
            <button id="nuevoEnviar" style=" 
            background-color: #007BFF;
            color: white;
            align-items: center;
            margin-left: 19%;
            padding: 1.5% 25%;
            border: none;
            border-radius: 8px;">Enviar</button>
        `;
        
        // Agregar un controlador de eventos clic al nuevo botón "Enviar"
        const nuevoEnviar = document.getElementById("nuevoEnviar");
        nuevoEnviar.addEventListener("click", function () {
            // Crear el cuadro flotante
            const modal = document.createElement("div");
            modal.style.display = "flex";
            modal.style.justifyContent = "center";
            modal.style.alignItems = "center";
            modal.style.position = "fixed";
            modal.style.top = "0";
            modal.style.left = "0";
            modal.style.width = "100%";
            modal.style.height = "100%";
            modal.style.backgroundColor = "rgba(0, 0, 0, 0.5)";
            modal.style.zIndex = "999";

            // Crear el contenido del cuadro flotante
            const modalContent = document.createElement("div");
            modalContent.style.backgroundColor = "white";
            modalContent.style.padding = "20px";
            modalContent.style.borderRadius = "8px";
            modalContent.style.textAlign = "center";
            modalContent.style.boxShadow = "0px 0px 10px rgba(0, 0, 0, 0.5)";
            
            modalContent.innerHTML = "<p>Registro completo</p>";

            const imagen = document.createElement("img");
            imagen.src = "check.png";
            imagen.alt = "Imagen de registro completo";
            imagen.style.width = "50%";
            imagen.style.height = "7%";

            modalContent.appendChild(imagen);

            modal.appendChild(modalContent);

            document.body.appendChild(modal);

            setTimeout(function () {
                modal.style.display = "none";
                document.body.removeChild(modal);

                location.reload();
            }, 2000);
        });
    });
});
